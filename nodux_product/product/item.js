frappe.provide("erpnext.item");

frappe.ui.form.on("Item", {
  refresh: function(frm) {
		if (frm.doc.docstatus=='0') {
			frm.add_custom_button(__("Save"), function() {
				frm.events.save(frm);
			}).addClass("btn-primary");
		}
		frm.refresh_fields();
	},

  save: function(frm) {
		return frappe.call({
			doc: frm.doc,
			method: "nodux_product.product.item.validate",
			freeze: true,
			callback: function(r) {
				frm.refresh_fields();
				frm.refresh();
			}
		})
	},

	customer: function(frm) {
		if (frm.doc.customer){
			frm.set_value("customer_name", frm.doc.customer);
			frm.set_value("due_date", frappe.datetime.nowdate());
		}
		frm.refresh_fields();
	},

  item_code: function(frm) {
    if (frm.doc.item_code == frm.doc.item_name){
      frm.set_value("item_name", "");
      frm.set_value("description", "");
    }
	},

  list_price: function(frm) {
    frm.set_value("list_price_with_tax", frm.doc.list_price);
		if (frm.doc.list_price){
      return calculate_prices(frm)
		}
		frm.refresh_fields();
	},

  cost_price: function(frm) {
    frm.set_value("cost_price_with_tax", frm.doc.cost_price);
		if (frm.doc.cost_price){
      return calculate_prices(frm)
		}
		frm.refresh_fields();
	},

  list_price_with_tax: function(frm) {
		if (frm.doc.list_price_with_tax){
      return calculate_prices_with_tax(frm)
		}
		frm.refresh_fields();
	},

  cost_price_with_tax: function(frm) {
		if (frm.doc.cost_price_with_tax){
      return calculate_prices_with_tax(frm)
		}
		frm.refresh_fields();
	},

  item_group: function(frm){
    var group = frm.doc.item_group;
    if (group){
      frappe.db.get_value("Item Group", {"item_group_name": group}, "tax", function(r) {
          frm.set_value("tax", r.tax);
      })
    }
    frm.refresh_fields();
  },

  tax: function(frm) {
    var cost_price_with_tax = 0
    var list_price_with_tax = 0
		if (frm.doc.tax){
        if (frm.doc.tax == "IVA 12%"){
          if (frm.doc.cost_price){
            cost_price_with_tax = flt(frm.doc.cost_price) * flt(1+0.12)
          }
          if (frm.doc.list_price){
            list_price_with_tax = flt(frm.doc.list_price) * flt(1+0.12)
          }
        }
        if (frm.doc.tax == "IVA 0%"){
          if (frm.doc.cost_price){
            cost_price_with_tax = flt(frm.doc.cost_price)
          }
          if (frm.doc.list_price){
            list_price_with_tax = flt(frm.doc.list_price)
          }
        }
        if (frm.doc.tax == "No aplica impuestos"){
          if (frm.doc.cost_price){
            cost_price_with_tax = flt(frm.doc.cost_price)
          }
          if (frm.doc.list_price){
            list_price_with_tax = flt(frm.doc.list_price)
          }
        }

        frm.set_value("cost_price_with_tax", cost_price_with_tax);
        frm.set_value("list_price_with_tax", list_price_with_tax);

		}
		frm.refresh_fields();
	},
});

var calculate_prices = function(frm) {
	var doc = frm.doc;
	doc.list_price_with_tax = 0;
	doc.cost_price_with_tax = 0;
  cost_price_with_tax = 0;
  list_price_with_tax = 0;

	if(doc.tax) {
    if (doc.tax == "IVA 12%"){
      if (doc.cost_price){
        cost_price_with_tax = flt(doc.cost_price) * flt(1+0.12)
      }
      if (doc.list_price){
        list_price_with_tax = flt(doc.list_price) * flt(1+0.12)
      }
    }
    if (doc.tax == "IVA 0%"){
      if (doc.cost_price){
        cost_price_with_tax = flt(doc.cost_price)
      }
      if (doc.list_price){
        list_price_with_tax = flt(doc.list_price)
      }
    }
    if (doc.tax == "No aplica impuestos"){
      if (doc.cost_price){
        cost_price_with_tax = flt(doc.cost_price)
      }
      if (doc.list_price){
        list_price_with_tax = flt(doc.list_price)
      }
    }
    doc.list_price_with_tax = list_price_with_tax;
    doc.cost_price_with_tax = cost_price_with_tax;
	}else{
    if (doc.group){
      frappe.db.get_value("Item Group", {"item_group_name": doc.group}, "tax", function(r) {
          if (r.tax == "IVA 12%"){
            if (doc.cost_price){
              cost_price_with_tax = flt(doc.cost_price) * flt(1+0.12)
            }
            if (doc.list_price){
              list_price_with_tax = flt(doc.list_price) * flt(1+0.12)
            }
          }else if (r.tax == "IVA 0%"){
            if (doc.cost_price){
              cost_price_with_tax = flt(doc.cost_price)
            }
            if (doc.list_price){
              list_price_with_tax = flt(doc.list_price)
            }
          }else if (r.tax == 'No aplica impuestos'){
            if (doc.cost_price){
              cost_price_with_tax = flt(doc.cost_price)
            }
            if (doc.list_price){
              list_price_with_tax = flt(doc.list_price)
            }
          }
      })
      doc.list_price_with_tax = list_price_with_tax;
      doc.cost_price_with_tax = cost_price_with_tax;
    }
  }
	refresh_field('list_price_with_tax')
	refresh_field('cost_price_with_tax')
}

var calculate_prices_with_tax = function(frm) {
	var doc = frm.doc;
	doc.list_price = 0;
	doc.cost_price = 0;
  cost_price = 0;
  list_price = 0;

	if(doc.tax) {
    if (doc.tax == "IVA 12%"){
      if (doc.cost_price_with_tax){
        cost_price = flt(doc.cost_price_with_tax) / flt(1+0.12)
      }
      if (doc.list_price_with_tax){
        list_price = flt(doc.list_price_with_tax) / flt(1+0.12)
      }
    }
    else if (doc.tax == "IVA 0%"){
      if (doc.cost_price_with_tax){
        cost_price = flt(doc.cost_price_with_tax)
      }
      if (doc.list_price_with_tax){
        list_price = flt(doc.list_price_with_tax)
      }
    }
    else if (doc.tax == "No aplica impuestos"){
      if (doc.cost_price_with_tax){
        cost_price = flt(doc.cost_price_with_tax)
      }
      if (doc.list_price_with_tax){
        list_price = flt(doc.list_price_with_tax)
      }
    }
    else {
      if (doc.cost_price_with_tax){
        cost_price = flt(doc.cost_price_with_tax)
      }
      if (doc.list_price_with_tax){
        list_price = flt(doc.list_price_with_tax)
      }
    }
    doc.list_price = list_price;
    doc.cost_price = cost_price;
	}else{
    if (doc.group){
      frappe.db.get_value("Item Group", {"item_group_name": doc.group}, "tax", function(r) {
          if (r.tax == "IVA 12%"){
            if (doc.cost_price_with_tax){
              cost_price = flt(doc.cost_price_with_tax) / flt(1+0.12)
            }
            if (doc.list_price_with_tax){
              list_price = flt(doc.list_price_with_tax) / flt(1+0.12)
            }
          }else if (r.tax == "IVA 0%"){
            if (doc.cost_price_with_tax){
              cost_price = flt(doc.cost_price_with_tax)
            }
            if (doc.list_price_with_tax){
              list_price = flt(doc.list_price_with_tax)
            }
          }else if (r.tax == 'No aplica impuestos'){
            if (doc.cost_price_with_tax){
              cost_price = flt(doc.cost_price_with_tax)
            }
            if (doc.list_price_with_tax){
              list_price = flt(doc.list_price_with_tax)
            }
          }else{
            if (doc.cost_price_with_tax){
              cost_price = flt(doc.cost_price_with_tax)
            }
            if (doc.list_price_with_tax){
              list_price = flt(doc.list_price_with_tax)
            }
          }
      })
      doc.list_price = list_price;
      doc.cost_price = cost_price;
    }
  }
	refresh_field('list_price');
	refresh_field('cost_price');
}
